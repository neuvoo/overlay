# Copyright 2017 Neuvoo
# Distributed under the terms of the GNU General Public License v2

EAPI=6
RESTRICT="mirror"
PYTHON_COMPAT=( python{2_7,3_4,3_5,3_6} )

inherit distutils-r1

MY_PVR=${PVR//_}

DESCRIPTION="PAM support for Flask"
HOMEPAGE="https://pypi.python.org/pypi/Flask-SimpleLDAP"
SRC_URI="https://github.com/KujiraProject/Flask-PAM/archive/v${MY_PVR}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc examples"

RDEPEND="
	>=dev-python/flask-0.3[${PYTHON_USEDEP}]
	dev-python/pypam[${PYTHON_USEDEP}]
	dev-python/wheel[${PYTHON_USEDEP}]
	dev-python/jinja[${PYTHON_USEDEP}]
	dev-python/markupsafe[${PYTHON_USEDEP}]
	dev-python/itsdangerous[${PYTHON_USEDEP}]
	dev-python/werkzeug[${PYTHON_USEDEP}]
	doc? ( dev-python/sphinx[${PYTHON_USEDEP}] )"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]"

S="${WORKDIR}/Flask-PAM-${MY_PVR}"

python_prepare_all() {
	distutils-r1_python_prepare_all
}

python_compile_all() {
	use doc && esetup.py
}

python_install_all() {
	use examples && local EXAMPLES=( example/. )
	distutils-r1_python_install_all
}
