# Copyright 2017 Neuvoo
# Distributed under the terms of the GNU General Public License v2

EAPI=5
RESTRICT="mirror"
PYTHON_COMPAT=( python{3_4,3_5,3_6} )

inherit distutils-r1 eutils git-r3

DESCRIPTION="Yattag is a Python library for generating HTML or XML in a pythonic way."
HOMEPAGE="http://www.yattag.org"
EGIT_REPO_URI="https://github.com/leforestier/yattag.git"
EGIT_BRANCH="master"

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
  EGIT_COMMIT="${PVR}"
fi

SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc x86 ~amd64-linux ~x86-linux"
IUSE=""

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"

src_unpack() {

  git-r3_src_unpack

}
