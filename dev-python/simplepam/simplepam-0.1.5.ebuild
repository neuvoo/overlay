# Copyright 2017 Neuvoo
# Distributed under the terms of the GNU General Public License v2

EAPI=5
RESTRICT="mirror"
PYTHON_COMPAT=( python{3_4,3_5,3_6} )

inherit distutils-r1

DESCRIPTION="An interface to the Pluggable Authentication Modules (PAM) library on linux, written in pure python (using ctypes)"
HOMEPAGE="https://github.com/leonnnn/python3-simplepam"
SRC_URI="https://github.com/leonnnn/python3-simplepam/archive/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc x86 ~amd64-linux ~x86-linux"
IUSE=""

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]
		virtual/pam"

S="${WORKDIR}/python3-${PF}"
