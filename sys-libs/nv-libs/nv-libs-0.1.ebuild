# Neuvoo OS library ebuild
# $Header: viridior $

EAPI="6"

inherit git-r3 eutils

DESCRIPTION="Neuvoo libraries"
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="amd64 -*"
SLOT="0"

EGIT_REPO_URI="https://bitbucket.org/neuvoo/nv-libs"

EGIT_BRANCH="master"

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
  EGIT_COMMIT="${PVR}"
fi

IUSE=""

RDEPEND=""

src_unpack() {

  git-r3_src_unpack

}

src_install() {

  into /

  # shared files
  insinto usr/share/neuvoo/lib
  insopts -m555
  doins -r shell

}
