# Copyright 2017 Neuvoo
# Distributed under the terms of the GNU General Public License v2
# edited from fedora-gentoo overlay; https://github.com/yarda/gentoo-fedora/tree/master/sys-libs/libuser

EAPI="6"
PYTHON_COMPAT=( python{2_7,3_4,3_5,3_6} )

inherit eutils python-r1

DESCRIPTION="A user and group account administration library"
HOMEPAGE="https://pagure.io/libuser"
SRC_URI="https://releases.pagure.org/libuser/libuser-${PV}.tar.xz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND=""
DEPEND="
	dev-libs/glib:2
	app-text/linuxdoc-tools
	virtual/pam
	dev-libs/popt
	dev-lang/python
	dev-libs/cyrus-sasl
	net-nds/openldap
	sys-apps/unscd
	dev-libs/openssl"

src_install() {
	emake DESTDIR="${D}" install || die

	# remove la files
	find "${ED}" -name '*.la' -exec rm -f {} +
}
