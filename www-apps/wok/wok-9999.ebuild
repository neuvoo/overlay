# Copyright 2017 Neuvoo
# Distributed under the terms of the GNU General Public License v2

EAPI="6"
PYTHON_COMPAT=( python2_7 )

inherit python-single-r1

DESCRIPTION="An HTML5 management interface for KVM"
HOMEPAGE="https://github.com/kimchi-project/wok"

LICENSE="LGPL-3"
SLOT="0"
IUSE=""

if [ ${PV} = "9999" ]; then
	KEYWORDS="~amd64 ~x86"
	inherit git-r3
	EGIT_REPO_URI="https://github.com/kimchi-project/wok.git"
	EGIT_COMMIT="${PV}"
else
	KEYWORDS="amd64 x86"
	SRC_URI="https://github.com/kimchi-project/wok/archive/${PV}.tar.gz -> wok-${PV}.tar.gz"
fi

DEPEND="
	dev-libs/libxslt
	>=dev-python/cheetah-2.0.4	
	dev-python/cherrypy
	dev-python/ipaddr
	dev-python/jsonschema
	dev-python/lxml
	dev-python/m2crypto
	dev-python/psutil
	dev-python/pypam
	>=dev-lang/python-2.7[sqlite]
	dev-python/pillow
	dev-python/websockify
	media-fonts/fontawesome
	dev-texlive/texlive-fontsextra
	virtual/httpd-basic"
RDEPEND="${DEPEND}"


HOMEDIR="${ROOT}var/lib/${PN}"
DOCS="AUTHORS COPYING INSTALL README NEWS ChangeLog VERSION"

src_configure() {

	./autogen.sh --system

}

src_compile() {

	emake

}

src_install() {

	keepdir "/var/lib/${PN}"
	dodir "/var/run/${PN}"
	dodir "/etc/${PN}"

	emake DESTDIR=${D} install

	newinitd "${FILESDIR}/wokd.initd" wokd
	newconfd "${FILESDIR}/wokd.confd" wokd

}
