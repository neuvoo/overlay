# Copyright 2017 Neuvoo
# Distributed under the terms of the GNU General Public License v2
# edited from chaos overlay; https://github.com/travisghansen/chaos/blob/master/www-apps/kimchi/kimchi-9999.ebuild

EAPI="6"
PYTHON_COMPAT=( python2_7 )

inherit eutils python-single-r1

DESCRIPTION="hypervisor guest management plugin for Wok"
HOMEPAGE="https://github.com/kimchi-project/kimchi"

LICENSE="LGPL-3"
SLOT="0"
IUSE=""

if [ ${PV} = "9999" ]; then
	KEYWORDS="~amd64 ~x86"
	inherit git-r3
	EGIT_REPO_URI="https://github.com/kimchi-project/kimchi.git"
	EGIT_COMMIT="${PV}"
else
	KEYWORDS="amd64 x86"
	SRC_URI="https://github.com/kimchi-project/kimchi/archive/${PV}.tar.gz -> ${P}.tar.gz"
fi

S="${WORKDIR}/kimchi-${PV}"

DEPEND="
	www-apps/wok
	dev-python/pyparted
	dev-python/python-ethtool
	dev-python/libvirt-python"
RDEPEND="${DEPEND}
	dev-python/paramiko
	sys-devel/gettext
	www-apps/novnc"

HOMEDIR="${ROOT}var/lib/${PN}"
DOCS="AUTHORS COPYING INSTALL README NEWS ChangeLog VERSION"

src_unpack() {

    unpack ${A}
	cd "${S}"
	epatch "${FILESDIR}/utils-detect-libvirtd.patch"

}

src_configure() {

	./autogen.sh --system

}

src_compile() {

	emake

}

src_install() {

	keepdir "/var/lib/${PN}"
	dodir "/var/run/${PN}"
	dodir "/etc/${PN}"

	emake DESTDIR=${D} install

}
