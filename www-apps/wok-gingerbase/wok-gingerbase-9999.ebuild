# Copyright 2017 Neuvoo
# Distributed under the terms of the GNU General Public License v2

EAPI="6"
PYTHON_COMPAT=( python2_7 )

inherit python-single-r1

DESCRIPTION="hypervisor management plugin for Wok"
HOMEPAGE="https://github.com/kimchi-project/gingerbase"

LICENSE="LGPL-3"
SLOT="0"
IUSE=""

if [ ${PV} = "9999" ]; then
	KEYWORDS="~amd64 ~x86"
	inherit git-r3
	EGIT_REPO_URI="https://github.com/kimchi-project/gingerbase.git"
	EGIT_COMMIT="${PV}"

else
	KEYWORDS="amd64 x86"
	SRC_URI="https://github.com/kimchi-project/gingerbase/archive/${PV}.tar.gz -> ${P}.tar.gz"
fi

S="${WORKDIR}/gingerbase-${PV}"

DEPEND="
	www-apps/wok
	dev-python/configobj
	dev-python/pyparted"
RDEPEND="${DEPEND}"


HOMEDIR="${ROOT}var/lib/${PN}"
DOCS="AUTHORS COPYING INSTALL README NEWS ChangeLog VERSION"

src_configure() {

	./autogen.sh --system

}

src_compile() {

	emake

}

src_install() {

	keepdir "/var/lib/${PN}"
	dodir "/var/run/${PN}"
	dodir "/etc/${PN}"

	emake DESTDIR=${D} install

}
