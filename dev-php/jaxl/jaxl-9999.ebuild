# Copyright 2013 Neuvoo Hosting
# Distributed under the terms of the GNU General Public License v2

EAPI="4"
PHP_EXT_NAME="jaxl"
PHP_EXT_SKIP_PHPIZE="YES"
PHP_EXT_INI="no"
DOCS="docs examples"

USE_PHP="php5-3 php5-4 php5-5"

inherit git-2 php-ext-source-r2

DESCRIPTION="Jaxl is an asynchronous, non-blocking I/O, event based PHP library for writing custom TCP/IP client and server implementations."
HOMEPAGE="http://jaxl.readthedocs.org/"

EGIT_REPO_URI="https://github.com/abhinavsingh/JAXL.git"

#if not using 9999, grab appropriate branch
if [ ${PV} != "9999" ]; then
        EGIT_BRANCH="${PV}"
fi

LICENSE="as-is"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="nomirror strip"
RDEPEND=""
DEPEND=">=dev-lang/php-5.3.0"

PHP_LIB_DIR="/usr/share/php/${PN}"

S="${WORKDIR}/JAXL"
PHP_EXT_S="${S}"

pkg_setup() {
    PHP_VER=$(best_version =dev-lang/php-5*)
    PHP_VER=$(echo ${PHP_VER} | sed -e's#dev-lang/php-\([0-9]*\.[0-9]*\)\..*#\1#')
    QA_TEXTRELS="${EXT_DIR/\//}/${PHP_EXT_NAME}.so"
    QA_EXECSTACK="${EXT_DIR/\//}/${PHP_EXT_NAME}.so"
}

src_unpack() {
	git-2_src_unpack

	cd "${S}"
    mkdir modules
    local slot orig_s="${PHP_EXT_S}"
    for slot in $(php_get_slots); do
        PHP_VER=$(echo ${slot} | sed 's/php\([0-9]\.[0-9]\)/\1/')
        mv ${PHP_EXT_NAME} "modules/${PHP_EXT_NAME}.so"
        cp -r "${orig_s}" "${WORKDIR}/${slot}" || die "Failed to copy source ${orig_s} to PHP target directory"
    done
}

src_install() {
	php-ext-source-r2_createinifiles

	#documents
	local doc
	for doc in ${DOCS}; do
		[[ -d ${doc} ]] && dodir ${doc}
	done
}

pkg_config () {
    einfo "Please refer to the installation instructions"
    einfo "in /usr/share/doc/${CATEGORY}/${P}/README.md."
}
