# Neuvoo OS initramfs ebuild
# $Header: viridior $

EAPI="6"

inherit git-r3 eutils

DESCRIPTION="Neuvoo initramfs"
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="amd64 -*"
SLOT="0"

EGIT_REPO_URI="https://bitbucket.org/neuvoo/initramfs"

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
  EGIT_COMMIT="v${PVR}"
fi

IUSE="lz4 lzma lzo musl zfs xz"

RDEPEND="
  dev-util/strace
  sys-libs/neuvoo-libs
  sys-apps/neuvoo-installer
  lz4? ( app-arch/libarchive[static-libs] dev-util/cmake app-arch/lz4 )
  lzma? ( app-arch/xz-utils[static-libs] )
  lzo? ( dev-libs/lzo[static-libs] app-arch/lzop )
  musl? ( sys-libs/musl )
  xz? ( app-arch/xz-utils[static-libs] )
  zfs? ( sys-kernel/spl sys-fs/zfs sys-fs/zfs-kmod[rootfs] )
"

src_unpack() {

  git-r3_src_unpack

}

src_install() {

  # docs
  dodoc README.md

  # shared files
  insinto usr/share/neuvoo/initramfs
  insopts -m444
  doins -r configs || die
  doins -r scripts || die
  insopts -m544
  doins module || die

}
