# Neuvoo OS kernel ebuild
# $Header: viridior $

EAPI="6"

inherit git-r3 eutils

DESCRIPTION="Neuvoo Linux Kernel (Source)"
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="amd64 -*"
SLOT="0"

EGIT_REPO_URI="https://bitbucket.org/neuvoo/neuvoo-kernel"

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
  EGIT_BRANCH="${PV}"
  EGIT_COMMIT="${PR}"
fi

IUSE=""

RDEPEND="sys-libs/neuvoo-libs"

src_unpack() {

  git-r3_src_unpack

}

src_install() {

  kernel-2_src_install

}

pkg_postinst() {

  einfo "Don't forget to run post install scripts under /usr/src/linux/nv/"

}
