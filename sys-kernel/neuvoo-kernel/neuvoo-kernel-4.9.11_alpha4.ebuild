# Neuvoo OS kernel ebuild
# $Header: viridior $

EAPI="6"

DESCRIPTION="Neuvoo Linux Kernel (Binary)"
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="amd64 -*"
SLOT="0"

# Don't use Gentoo mirrors
RESTRICT="mirror"

SRC_URI="
  host? ( https://bitbucket.org/neuvoo/neuvoo/raw/master/os/boot/linux-${PVR}-neuvoo-host.tar.xz )
  vm? ( https://bitbucket.org/neuvoo/neuvoo/raw/master/os/boot/linux-${PVR}-neuvoo-vm.tar.xz )
"

IUSE="aufs host vm zfs"

REQUIRED_USE="|| ( host vm )"

RDEPEND="
  sys-libs/neuvoo-libs
  aufs? ( sys-fs/aufs4[static-libs] )
  zfs? ( =sys-fs/zfs-0.6.5.11[rootfs] =sys-fs/zfs-kmod-0.6.5.11 =sys-kernel/spl-0.6.5.11[static-libs] )
"

