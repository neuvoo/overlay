# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit multilib

DESCRIPTION="XMLRPC-Erlang with IP, Ruby and Xmerl 1.x patches"
HOMEPAGE="http://www.ejabberd.im/ejabberd_xmlrpc"
SRC_URI="http://www.ejabberd.im/files/contributions/xmlrpc-1.13-ipr2.tgz"

LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="dev-lang/erlang"
RDEPEND="${DEPEND}"
S=${WORKDIR}/xmlrpc-${PV}/src/

src_install() {
	local ERL_LIBDIR="/usr/$(get_libdir)/erlang/lib/"

	dodir ${ERL_LIBDIR}/xmlrpc/ebin
	insinto ${ERL_LIBDIR}/xmlrpc/ebin
	doins ../ebin/*.beam

	dodir ${ERL_LIBDIR}/xmlrpc/src
	insinto ${ERL_LIBDIR}/xmlrpc/src
	doins *.erl
}

pkg_postinst() {
        elog "Please note this is a patched version of Erlang's xmlrpc library"
	elog "for Ejabberd."
}
