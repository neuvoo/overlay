# README #

## Setup 
### /etc/layman/layman.conf ###
```text
conf_type : repo.conf
```

### /etc/portage/repos.conf/neuvoo.conf ###
```text
[neuvoo]
location = /usr/local/overlay/neuvoo
sync-type = git
sync-uri = http://bitbucket.org/neuvoo/neuvoo-overlay.git
auto-sync = yes
```

## Layout
### Packages ###

The Neuvoo overlay uses the standard Gentoo-style package naming convention and layout and conforms as much as logically possible to already existing package & directory structures.
  
### Profiles ###
```text
profiles/neuvoo/$VERSION/
```

After using Gentoo and Funtoo style profile structure, Neuvoo has decided to use the Funtoo-style profile structure with our own Neuvoo-specific changes in order to maximize the functionality of the Portage system to simplify the overall install, update, and management processes.
Only Neuvoo unique profile structures will be described here.

#### platforms ###
```text
platform/
```

Select ONE desired platform.  The select platform will inherit some of the other branches of software by default with the desire to provide the MINIMAL software necessary to provide a bootable & functional system for as many reasonable systems architectures as possible.

#### hardware ####
```text
hardware/
```

An many cases, additional software selections may be needed for unique hardware situations.  Select only the hardware needed.
