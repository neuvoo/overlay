# Copyright 2009-2013 Neuvoo Hosting
# Distributed under the terms of the GNU General Public License v2
# $Header: viridior $

EAPI=5

inherit git-2 eutils

DESCRIPTION="Neuvoo Hosting tools for managing minecraft servers."
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="~amd64"
SLOT="0"

EGIT_REPO_URI="https://bitbucket.org/neuvoo/service-minecraft/"
SRC_URI=""

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
    EGIT_COMMIT="v${PVR}"
fi

IUSE="console"

DEPEND="
	sys-apps/neuvoo-system-tools
"

RDEPEND="
	|| (
		>=virtual/jdk-1.7
		>=virtual/jre-1.7
	)
	console? (
		app-admin/sudo
		app-misc/tmux
	)
"

src_unpack() {
	if use console; then
		mkdir -p "${S}/minecraft"
		cp -Rv "${DISTDIR}/console" "${S}/minecraft/bin"
	fi
}

pkg_setup() {

	[[ "${NEUVOO_DIR}" ]] || die "NEUVOO_DIR not set!  Did you update your environment?"

}

src_install() {

	#executables
	if use console; then
		exeinto ${NEUVOO_DIR}/minecraft/bin/
		exeopts -m755 ${S}/minecraft/bin/*
		dobin ${S}/minecraft/bin/* || die "Could not install executables."
	fi

}

pkg_postinst() {

	ewarn "console: each client must still reference a jar in order to run."

}
