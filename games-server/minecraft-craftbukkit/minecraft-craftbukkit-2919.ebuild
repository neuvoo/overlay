# Copyright 2013 Neuvoo Hosting
# Distributed under the terms of the GNU General Public License v2
# $Header: viridior $

EAPI=4
MC_URI="http://dl.bukkit.org/downloads/craftbukkit/get/02391_1.6.4-R2.1/craftbukkit-dev.jar"
#recommended, beta, development
MC_TYPE="development"
MC_PV="1.6.4-R2.1"
MC_JAR="${PN}-${PVR}-${MC_TYPE}-${MC_PV}.jar"

inherit eutils

DESCRIPTION="Minecraft Craftbukkit implementation."
HOMEPAGE="http://dl.bukkit.org/downloads/craftbukkit/"
SRC_URI="${MC_URI} -> ${MC_JAR}"
LICENSE="GPL-3"
SLOT="${MC_PV}"

KEYWORDS="~amd64 ~x86"

DEPEND="
		sys-apps/neuvoo-system-tools[neuvoo_services_minecraft]
"

RDEPEND="
		|| ( 
			>=virtual/jre-1.7
			>=virtual/jdk-1.7
		)
"

S="${WORKDIR}"

src_unpack() {
	mkdir -p "${S}/minecraft/jar"
	cp "${DISTDIR}/${MC_JAR}" "${S}/minecraft/jar/"
}

src_install() {
	if [[ ! "${NEUVOO_DIR}" ]] || [[ ! "${NEUVOO_MC_DIR}" ]]; then
		echo "NEUVOO environmental variables are not defined!"
		exit 1
	fi

	local basedir="${NEUVOO_DIR}/${NEUVOO_MC_DIR}"

	dodir ${basedir}
	cp -R "${S}/minecraft/." "${D}/${basedir}/" || die "Install failed"
}
