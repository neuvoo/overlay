# Copyright 2013 Neuvoo Hosting
# Distributed under the terms of the GNU General Public License v2
# $Header: viridior $

EAPI=4
MC_JAR="minecraft-server-${PVR}.jar"

inherit eutils

DESCRIPTION="Minecraft Server."
HOMEPAGE="http://minecraft.net"
SRC_URI="https://s3.amazonaws.com/Minecraft.Download/versions/${PVR}/minecraft_server.${PVR}.jar -> ${MC_JAR}
	https://www.neuvoo.org/download/distfiles/services/minecraft/neuvoo-minecraft-env.d"
LICENSE="GPL-3"
SLOT="${PVR}"

KEYWORDS="~amd64 ~x86"

DEPEND="
		sys-apps/neuvoo-system-tools[neuvoo_services_minecraft]
"

RDEPEND="
		|| ( 
			>=virtual/jre-1.7
			>=virtual/jdk-1.7
		)
"


S="${WORKDIR}"

src_unpack() {
	mkdir -p "${S}/minecraft/jar"
	cp "${DISTDIR}/${MC_JAR}" "${S}/minecraft/jar/"
}

src_install() {
	if [[ ! "${NEUVOO_DIR}" ]] || [[ ! "${NEUVOO_MC_DIR}" ]]; then
		echo "NEUVOO environmental variables are not defined!"
		exit 1
	fi

	local basedir="${NEUVOO_DIR}/${NEUVOO_MC_DIR}"

	dodir ${basedir}
	cp -R "${S}/minecraft/." "${D}/${basedir}/" || die "Install failed"
}
