# Neuvoo OS system ebuild
# $Header: viridior $

EAPI="6"

inherit git-r3 eutils

DESCRIPTION="Neuvoo system tools"
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="amd64 -*"
SLOT="0"

EGIT_REPO_URI="https://bitbucket.org/neuvoo/nv-system"
EGIT_BRANCH="master"

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
  EGIT_COMMIT="${PVR}"
fi

IUSE=""

RDEPEND="
  sys-apps/nv
  sys-libs/nv-libs"

src_unpack() {

  git-r3_src_unpack

}

src_install() {

  into /

  # binaries
  dosbin sbin/nv

  # share
  insinto usr/share/neuvoo/system
  insopts -m444
  doins -r configs
  doins -r skel
  insopts -m544
  doins module

}
