# Neuvoo OS installer ebuild
# $Header: viridior $

EAPI="6"

inherit git-r3 eutils

DESCRIPTION="Neuvoo installer"
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="amd64 -*"
SLOT="0"

EGIT_REPO_URI="https://bitbucket.org/neuvoo/installer"

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
  EGIT_COMMIT="v${PVR}"
fi

IUSE="refind zfs"

RDEPEND="
  sys-apps/gptfdisk
  sys-fs/dosfstools
  sys-libs/efivar
  sys-boot/efibootmgr
  sys-libs/neuvoo-libs
  sys-apps/neuvoo-utils
  refind? ( sys-boot/refind )
  zfs? ( sys-kernel/spl sys-fs/zfs sys-fs/zfs-kmod )
"

src_unpack() {

  git-r3_src_unpack

}

src_install() {

  # shared files
  insinto usr/share/neuvoo/installer
  insopts -m444
  doins -r configs || die
  doins -r setup
  insopts -m544
  doins -r scripts || die
  doins module || die

}
