# Neuvoo OS genesis ebuild
# $Header: viridior $

EAPI="6"

inherit git-r3 eutils

DESCRIPTION="Neuvoo GENESIS"
HOMEPAGE="http://neuvoo.org/"
LICENSE="BSD"
KEYWORDS="amd64 -*"
SLOT="0"

EGIT_REPO_URI="https://bitbucket.org/neuvoo/genesis"

#if not using 9999, grab appropriate branch
if [[ ${PV} != "9999" ]] ; then
  EGIT_COMMIT="v${PVR}"
fi

IUSE=""

RDEPEND="sys-libs/neuvoo-libs"

src_unpack() {

  git-r3_src_unpack

}

src_install() {

  # docs
  dodoc README.md docs/*.md

  # shared
  insinto usr/share/neuvoo
  insopts -m544
  doins -r modules || die
  insinto usr/share/neuvoo/genesis
  insopts -m444
  doins -r configs
  insopts -m544
  doins -r build || die

}
